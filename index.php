<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Update_Fallback
 * @since Update Fallback 1.0
 */
get_header();
?>

<body>
    <span class="headerText">
        <?php echo get_bloginfo('url'); ?>
    </span>
    <br/>
    <span class="bodyText">
        <?php echo trim(get_bloginfo('title')); ?> is currently under maintenance.
    </span>
</body>
</html>