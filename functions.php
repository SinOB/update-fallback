<?php

/*
 * update-fallback functions 
 */

/** /
 * If the user is not an admin user force them to be logged out of the site.
 */
function update_fallback_force_logout() {
    if (!current_user_can('manage_options')) {
        wp_logout();
    }
}

add_action('bp_init', 'update_fallback_force_logout', 3);
